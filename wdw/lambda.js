let Swagger = require('swagger-client');

exports.handler = function (event, context, callback) {

    Swagger.http({
        url: `https://devapi.currencycloud.com/v2/authenticate/api`,
        method: 'post',
        query: {},
        headers: { "Accept": "application/json", "Content-Type": "application/x-www-form-urlencoded" },
        body: `login_id=1&api_key=12`
    }).then((response) => {
        // your code goes here
    }).catch((err) => {
        // error handling goes here
    });

    callback(null, { "message": "Successfully executed" });
}